---
layout: job_family_page
title: "Community Operations Manager"
---

The GitLab Community Operations Manager is responsible for developing and maintaining the infrastructure and resources to support the Community Relations team and the GitLab community at large. They create processes and documentation to enable the team to interact with the community, as well as help make it easier to provide feedback about GitLab. The Community Operations Manager reports to the [Director of Community Relations](https://about.gitlab.com/job-families/marketing/director-of-community-relations/).

## Responsibilities

- Maximize the Community Relations team's efficiency, productivity and performance.
- Act as a partner to all Program Managers in the Community Relations team to define, implement and refine KPIs/PIs to measure and report the success and effectiveness of our programs. In doing so, you will also be working closely with the Marketing Operations, and Data and Analytics teams.
- Define and maintain the tool stack required to measure and interact with the GitLab community.
- Be the DRI for the Community Relations team's webpages on about.gitlab.com. Work with the pertinent teams to produce relevant, enticing and up-to-date content for contributors.
- Support the Open Source and Education teams by processing program applications and renewals. Ultimately develop a process to fully automate them.
- Work with the Community Relations team’s Program Managers to produce regular, engaging content to highlight their programs and attract new contributors.
- Curate and maintain documentation for any team member to productively engage with the wider community.
- When necessary, engage with specialists within GitLab to provide responses and listen to our community’s feedback on The GitLab forum, the GitLab blog and Hackernews.

## Requirements

- Thrive at developing new ways and refining existing processes to enable teams to work more efficiently and provide a better community experience.
- Experience engaging with and supporting technical users and contributors in public online channels.
- You love working with the GitLab community, and Open Source and Education communities in general.
- Excellent written and spoken English. Proven track record of content creation such as public blog posts.
- Ability to use GitLab.
- You share our [values](https://about.gitlab.com/handbook/values), and work in accordance with those values.

## Levels

### Junior Community Operations Manager

#### Job Grade

The Junior Community Operations Manager is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Junior Community Operations Manager Responsibilities 
- Thrive at developing new ways.
- Refine existing processes.
- Enable teams to work more efficiently.

#### Junior Community Operations Manager Requirements 
- 1-2 years of experience 
- Demonstrated history of engaging with and supporting technical users and contributors in public online channels.
- Proven track record of content creation such as public blog posts.

### Community Operations Manager (Intermediate)

#### Job Grade

The Community Operations Manager (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Community Operations Manager (Intermediate) Responsibilities
- Extends the Junior Community Operations Manager responsibilities.
- Assist in developing common policies, processes and resources consistent across all community programs, with a handbook-first approach.
- Assess new tools to more effectively serve the GitLab community and to contribute to growth.
- Drive the design and implementation of all relevant community KPI dashboards.
- Provide advice and implement proposals to increase website traffic for community-oriented content. Measure the impact in number of contributions and program memberships.

#### Community Operations Manager (Intermediate) Requirements 
- 3-5 years of experience.

### Senior Community Operations Manager

#### Job Grade

The Senior Community Operations Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Community Operations Manager Responsibilities
- Extends the Community Operations Manager (Intermediate) responsibilities.
- Significantly contribute to automate all the Community Relations team's processes and worflows.
- Proactively work across functions with peers in other groups to ensure collaboration on shared goals. 
- Collaborate with all Community Relations team's Program Managers to create a framework for community programs, including templates and guidelines for landing pages, contributor events, membership, incentives, etc.

#### Senior Community Operations Manager Requirements 
- 6+ years of experience.

## Performance Indicators

- Time to approve community program applications
- Percentage of manual vs. automated application processes
- Number of community content impressions per month
- Member/contributor satisfaction for community programs

## Career Ladder

The next step in a Senior Community Operations Manager's career at GitLab would be the Staff Community Operations level, which is not yet defined. The next step after Staff would be [Director, Community Relations](/job-families/marketing/director-of-community-relations/).

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- A 45 minute interview with one of the Program Managers in the Community Relations team
- A 45 minute interview with our Director of Community Relations
- A 45 minute interview with our Director of Corporate Marketing
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).