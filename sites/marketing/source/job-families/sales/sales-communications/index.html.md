---
layout: job_family_page
title: "Sales Communications"
---

The Sales Communications team manage GitLab's commissions programs globally. They work cross-functionally and leverage systems for all related business processes. 

## Sales Communications Manager
The Sales Communications Manager reports to the [Senior Director, Field Enablement](/job-families/sales/director-of-field-enablement/#senior-director-field-enablement).

### Responsibilities
- Develop and manage effective sales enablement communication strategies and tactics including but not limited to newsletters, videos, memos, presentations, and more
- Collaborate and partner with GitLab Field Operations leaders and Sales and Customer Enablement team members to build and execute holistic, cross-program communications plans in support of prioritized field enablement-related initiatives
- Champion efforts to improve sales enablement communications via [GitLab’s handbook-first approach to learning and development](handbook/sales/field-operations/sales-enablement/#handbook-first-approach-to-gitlab-learning-and-development-materials)
- Develop and implement a strategy for soliciting feedback from GitLab Sales and Customer Success team members to inform enablement priorities and requirements
- Assist in communication of strategies or messages from senior leadership as needed
- Take on additional projects and responsibilities as needed

### Requirements
- BS/BA in communications or relevant field
- Proven experience as a communications specialist, preferably with high-tech B2B sales audiences
- Experience in copywriting and editing
- Strong project management skills and attention to detail needing minimal supervision
- Proven experience developing and executing effective sales newsletters is a huge plus
- Experience with Mailchimp (or a similar marketing/communications platform) is preferred 
- Working knowledge of Google docs; photo and video-editing software is an asset
- Excellent communication (oral and written) and presentation skills
- Outstanding organizational and planning abilities
- Excellent team player and ability to effectively collaborate with others
- Experience in web design and content production is a plus
- Knowledge of the software development life cycle, DevOps, and/or open source software is preferred
- You share our [values](/handbook/values), and work in accordance with those values.
- Ability to use GitLab

### Job Grade
The Sales Communications Manager (Intermediate) is [6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Performance Indicators
- Sales communication/newsletter open and click-through rates
- Field survey response rates
- Feedback from key stakeholders

### Carrer Ladder
The next steps for the Sales Communications Manager has yet to be defined.

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will meet with members of the [GitLab Field Operations team](/company/team/?department=field-operations),
* Then, candidates will meet with the Director of Sales and Customer Enablement, 
* Finally, candidates will meet with the VP of Field Operations.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
