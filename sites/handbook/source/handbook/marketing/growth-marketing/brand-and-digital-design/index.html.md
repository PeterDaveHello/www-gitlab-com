---
layout: handbook-page-toc
title: "Brand and Marketing Design Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Brand & Marketing Design Handbook
{:.no_toc}

Brand and Marketing Design is part of the Growth Marketing department. Our mission is to create brand and marketing experiences that attract, convert, nurture and delight customers throughout the customer journey, one MVC at a time.


### The Team

The Brand team’s primary responsibilities are:

- Developing and managing GitLabs brand and visual identity.
- Developing and managing campaign brands and identities.
- Enabling Everyone to Contribute with a consistent voice and look and feel.
- Content marketing design support, such as editorial and video content.

Quick Links:
- [Meet the Brand team](#meet-the-brand-team)
- [Brand standards and guidelines](/handbook/marketing/growth-marketing/brand-and-digital-design/brand-guidelines/)
- [Image guidelines](/handbook/marketing/growth-marketing/brand-and-digital-design/image-guidelines/)
- [Reviewing merge requests](/handbook/marketing/website/merge-requests/)


### What we do

The Team works with our GitLab peers and the wider GitLab community to rapidly co-design and release value generating experiences throughout the marketing funnel -from raising brand awareness and attracting strangers, to converting visitors to leads, leads to customers and customers to promoters:

- **Attract** - Blog, Keywords, Social Media
- **Convert** - Forms, CTAs, Landing Pages
- **Nurture** - Ebooks, White-papers, Case Studies, Webinars, Guides, Videos, Email Newsletters and Drip Campaigns, Free Trials, Demos
- **Delight** - Feedback Loops, Personalization


### Our focus

The Team's [mission](#mission), [work](#how-we-work), and solutions revolve around:

- **GitLab’s** **CREDIT Values** - We incorporate [Gitlab's CREDIT values](/handbook/values/#credit) into everything we do
- **Data-driven decisions**  - We use data to plan, design and measure success
- **Understanding the Problem** - We strive to understand the *who*, *what* and *why,* so we can best solve the how
- **Value-driven** **MVCs** - We rapidly plan, design and release results-driven MVCs, and measure our success
- **Customer focus** - We promote customer-centric strategies and solutions throughout the org
- **Conversion, conversion, conversion** - We optimize for conversion throughout the buyer journey
- **Story telling** - We build our brand through compelling story telling
- **Decisive Collaboration** - We facilitate an open and inclusive, decision-driven design process
- **Cohesive** **Experiences** - We build cohesive, omni-channel experiences across diverse media and devices, one MVC touchpoint at a time
- **Everyone Can Contribute** - We support GitLab’s Everyone Can Contribute model by creating contribution paths aligned to our brand and business goals
- **Efficiency via DRY** - In the spirit of Don’t Repeat Yourself (DRY), we leverage strategic reuse to promote consistency and speed



### Contact us

- [Slack](https://gitlab.com/gitlab-com/marketing/growth-marketing)
- [Email Brand and Digital](mailto:brand-and-digital@gitlab.com)

# Mission

Our mission is to rapidly release value generating MVCs, together. To realize our mission, we strive to be collaborative, innovative, informative, efficient, and results-driven in all things we do:

- **Innovative** - We emphasize GitLab’s disruptive business and product innovation, and embrace new ideas and ways to achieve our goals.
- **Informative** - We support evidence-based solutions via brand and conversion design best practices, competitive and user research, and customer feedback loops.
- **Efficient** - We solve efficiently “boring” solutions, Lean UX tools and techniques, and design reuse (e.g. design systems, reusable assets, and self-serve templates).
- **Results focused** - We deliver results fast by rapidly releasing value-generating, customer-centric MVCs and measuring success via performance indicators (PIs).
- **Collaborative** - We facilitate an open and iterative design process, that welcomes contributions from Gitlabbers and the wider GitLab community end to end.


# Working with us

To work with Brand and Digital, please:
- Use the following issue templates and labels to submit your issue requests
- Submit issue requests with adequate lead time (the sooner the better). Two weeks is needed for general asks and more complicated requests will be triaged and timelines communicated. 


## Labels Overview
Please see the Growth Marketing Handbook section on [labels](/handbook/marketing/growth-marketing/#labels) for status and communication labels. Below is more detailed labels specific to Brand and Digital Design.



# How we work

See the Growth Marketing Handbook for the overview of our [process](https://about.gitlab.com/handbook/marketing/growth-marketing/#process) 

#### Sprint planning
Before the sprint starts, Brand and Digital teams break high-level issue requests into task-based implementation issues, as needed. Implementation issues are assigned, weighted and related to other issues to denote relationships and dependencies (i.e. blocking issues).

- Production Issues: 
    - [**Strategy Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-strategy) - Issue to define strategic brief that defines project hypothesis, scope, business and brand requirements, target audience, CTAs and any other information key to success.
    - [Conversion Checklist Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-conversion) - Issue that’s opened at the start of a conversion project, completed by design, development and testers throughout the project, and closed at the end of the project.
    - [**Prototyping Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-prototyping) - Issue to facilitate co-design sessions using experience mapping and or lo- to mid-fi prototyping to rapidly align on big picture experience and value-driven MVCs.
    - [**Visual Design Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-visualdesign) - Issue to theme the approved conversion design prototypes.
    - [**Development Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-developement) - Issue to develop the approved design.
    - [**Testing Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-testing) - Issue to define, conduct and report outcome of tests.
- Weighting
    - We use issue weight to plan and manage our work better. Team members are not held to weights, they’re simply a planning and learning tool. Brand and Digital weights are based on the following point scale: 1pt = .5 day
- Issue Assignments
    - All sprinted implementation issues are assigned to designer or developer. He or she is responsible for leading the issue end-to-end, including facilitating cross-functional completion, keeping the SSOT it up to date, responding to contributions, and completing requirements.
- Right-sizing Sprints
    - We strive to plan sprints we can complete. When we don’t, that is ok, so long as we learn from the experience and continually improve.  In order to achieve predictable sprint outcomes we focus on:
        - Can I complete this work in a week?
        - Have we been under or over estimating this type of work? Should we adjust the weight accordingly?
        - Do I need to break this MVC into smaller MVCs?
        - Do I anticipate any blockers, like cross-functional dependencies or technical challenges? If so, are they manageable or should I pivot to an alternate priority?

#### Sprint cycle

- **Weekly Meetings**
    - **Sprint Kick-off** - At the beginning of each week, we kick-off the sprint and each team member:
        - Shares their priorities for the weekly sprint in the meeting agenda.
        - Notes any blockers.
    - **Slack** **Stand-ups**
        - We’re experimenting with async Slack stand-ups. If you’re not participating in Slack stand-ups and would like to, please reach out to your leader.
    - **1:1’s**
        - Leaders check-in with team members on sprinted projects to provide feedback, address blockers and or pivot project priorities as needed.
        - Its recommended team members issue boards be used to facilitate these discussions.
    - **Team Reviews**
        - Gathering iterative feedback is an essential part of the design and development process. Team reviews, allow team members to get input and ideas before work begins, quickly solicit feedback on works in progress (WIP), and validate final solutions. Because GitLab is an all-remote company, team reviews are also important for building shared understanding with the broader team and company.
            - Managers will incorporate peer reviews into their team processes, and provide feedback on MVC scope, processes, and WIP and final designs and development solutions.
            - Team members will present at least once a fiscal quarter, and record and share with the Marketing Growth team for awareness and async contribution.
            - Optionally, and only with presenter approval,  team review recordings can be posted  to GitLab Unfiltered.
            - Be sure to frame discussions around the customer and the problem being solved, not the ”pretty” visual design or functionality.  As the discussion unfolds, continually tie everything back to how we can best achieve Gitlab (ex: conversion) and target audience needs (ex: easily getting valuable content from trustworthy source).
    - **Demo Day!**
        - Team members showcase something they have completed during the sprint.
        - Peers have the opportunity for Q&A async, or verbally if time permits.
- **Close out** - We close out the week's milestone each week by processing:
    - Closed issues - Closing completed issues.
    - Rollover issues - Moving issues forward if they were not closed out and comment why the rollover is taking place. Issues that consistently rollover may require attention.


### Strategy


#### Define the opportunity

- Work with your stakeholders to clearly define *who* we're solving for, *what we want to achieve*, and *why* we’re solving for it.
- Help your stakeholders define the *who*/*what*/*why* as a user story. For example, "As a (who), I want (what), so I can (why/value)." If you’re asked to implement a non-evidence-based *how* (i.e. a specific solution), then ask the requestor to focus on the who/what/why, so everyone can work together to find the best *how*.
- Help stakeholders define [MVC](/handbook/values/#minimal-viable-change-mvc) success criteria, prioritizing MVC “must-haves” and non-MVC “should-haves” and “could-haves.” (Note that this success criteria is subject to change based on new learning from the iterative design process and customer feedback.)


### Design process


#### Before you design

**Generate ideas**
Part of the role of Brand and Digital is to lead and facilitate idea generation with stakeholders. We are all very busy working with stakeholders to solve known problems, but remember that there are also undiscovered problems out there that are definitely worth solving. Here are a few activities and resources to inspire you!

- Run a sync, async, or design session to generate ideas. Define the scope and goals for the session, and invite diverse participants for the best results.
- Reach out to other GitLabbers who may represent the target audience or simply have unique perspective and insights, by inviting them to meetings or via Slack, to generate new idea and or get feedback on existing ideas.
- Spin up a sync or async session to share competitive and comparative solutions, and then use them as an “iteration 0” like/dislike session with stakeholders.

#### Understand the space

- Consider lightweight competitive research an analysis to inform your work.
    - Google and or social search the subject you’re working on
    - Checkout competitor and comparative solutions
    - Put yourself in the target audiences shoes, as you get a sense of the competitive and market landscape.
    - Identify “boring” design conventions customers expect, such as interactive patterns or subject matter iconography, that we can embrace for marketing and release speed reasons. Only stray from industry conventions with strategic intent, such as capitalizing on [disruptive innovation](https://www.economist.com/the-economist-explains/2015/01/25/what-disruptive-innovation-means) opportunities.
    - Screen capture competitive and comparative solutions, you can use to generate ideas.
- Consider creating user flows or journey maps to help ensure you and stakeholders understand how the touchpoint you’re working on fits into the broader buyer journey. Where are prospects coming from? Where will they go next? How do we optimize the overall experience for conversion?
- Read the content you’re designing, so you can maximize the message with meaningful photos, illustrations and diagrams that are informative and engaging.

#### Investigate possible dependencies

It is our responsibility as Brand and Digital to ensure the touchpoints and experiences we deliver are well integrated into GitLab’s experiences.

- Proactively reach out to other Brand and Digital GitLabbers to align your work to theirs and ensure the solution delivered is on brand and conversion optimized.
- Identify the DRIs, cross-discipline peers and stakeholders early and make sure they’re aware of your work, their role and that they have what they need from you to contribute and avoid delaying your delivery.


### Design


#### Ideate and iterate

- Share design ideas in the lowest fidelity that still communicates your idea. To keep the cost of change low, only increase fidelity as design confidence grows and implementation requires.
- Ask for feedback from stakeholders throughout the design process to help refine your understanding of the problem and align on the best possible solution.
- Be sure to frame discussions around the customer and the problem being solved, not visual design or functionality. When presenting, walk through the proposed solution from the target audience’s point of view. As the discussion unfolds, continually tie everything back to how well proposed solutions achieve Gitlab and target audience goals.
- Ask for feedback from your Brand and Digital team via Slack or in a Team Design & Development Review <link> to help improve your work. At minimum, you'll get objective feedback and new ideas that lead to better solutions. You might also get context you didn’t know you were missing, such as related GitLab initiatives and solutions you can factor into your solution.
- Engage Digital peers early and often. Their insight into technical costs and feasibility is essential to determining viable designs and MVCs.
- Collaborate with your Content Marketing group early and often, to ensure design and copy work together to create a cohesive, compelling and conversion optimized experience.
- For important project, like a key content piece and or conversion path, include your leadership in feedback, as they might have input into the overall direction of the design or knowledge about initiatives that might impact your own work.
- Work with Growth Marketing peers to align on how to measure the success of the overall initiatives you’re a part of, and your MVCs.
- Make sure your solutions are aligned to design and dev standards and best practices, such as conversion best practices.

#### Refine MVC

- MVC issues have a tendency to expand in scope. Work with your stakeholders to revisit which aspects of the solution are “must haves” versus those that can be pushed until later. Document non-MVC requirements in new issues, and relate the new issues to the original issue. If you’re ever unsure how to split apart large issues, work with your leader.
- If development needs to begin before you have completed your design, and a planning pivot to another issue is not an option, then look for high confidence and low risk elements dev can start work on while you finish the remainder of the design. To mitigate these scenarios and the dev delays and waste they can create, everyone should work together to plan ahead.
- Devs should be able to build an MVC is one sprint. If an MVC is too large to build within one release, work with your leadership and peers to split the MVC into smaller MVCs that can be closed at the end of the sprint. Note: MVC may be completed in a sprint, but released later due to dependencies on other dev MVCs.

#### Final MVC

- After you've facilitated and open and inclusive process, present your final design solution in the Design tab.
- When sharing asynchronously in an issue, make sure your audience has the context necessary to understand how your proposal delivers on the who, what and why and our brand and business goals, and anything you need from them. Is it clear who will use the solution and how it meets Gitlab and target audience goals? Is this just the final interactive design or is visual design also final? Do you need feedback or assistance from stakeholders, like final content changes? To make reviewing easier, have you highlighted how you addressed final change requests, questions and concerns.
- Set the issue marketing status scope label to  `mktg-status::design-review` to open the solution up to final review and approval. If you are not able to get approval from your leadership, despite best efforts, do not let that hold up delivery.
- Anticipate questions that others might have, and try to answer them in your proposal comments. You don’t have to explain everything, but try to communicate a bit of your rationale every time you propose something. This is particularly important when proposing changes or challenging the status quo, because it reduces the feedback loop and time spent on unnecessary discussions. It also builds the UX Department’s credibility, because we deal with a lot of seemingly subjective issues.
- Keep the SSOT updated with what’s already agreed upon so that everyone can know where to look. This includes images or links to your design work.
- If you are proposing a solution that will introduce a design, or change an existing one, please consider the following:
    1. Will this design or interaction pattern be inconsistent with like experiences?
    2. Will like experiences need to be updated to match?
    3. Is upgrading this design or interaction pattern worth the cost?

#### Deliver

- Once your work is complete and all feedback is addressed, make sure that the issue description and SSOT are up to date, you’ve validated that you have followed any required best practices, and relevant parties are informed of what to do next.   
- As applicable, commit all final design assets and files to appropriate repositories.
- If the solution needs to be broken out into smaller issues for implementation, work with your Digital peer to do so.

#### Follow through

- Encourage developers to scope down features into multiple merge requests for an easier, more efficient review process.
- When breaking down features into multiple merge requests, consider how the UX of the application will be affected. If merging only a portion of the total changes will negatively impact the overall experience, consider using a feature branch or feature flag to ensure that the full UX scope ships together.
- When breaking solutions into smaller MVCs, make sure customers do not get fragmented or incomplete experiences. Make sure everyone understands the full picture so dependent MVCs are released together.
- Keep the issue description updated with the agreed-on scope and requirements, even if doesn’t impact your work. This is everyone’s responsibility. The issue description and design files must be the Single Source Of Truth (SSOT), not the discussion or individual comments. If the developer working on the issue ever has any questions on what they should implement, they can ask the designer to update the issue description with the design.
- For obvious changes, make the SSOT description update directly. [You don't need to wait for consensus](/handbook/values/). Use your judgement.
- When the issue is actively being worked on, make sure you are assigned and subscribed to the issue. Continue to follow both the issue and related merge request(s), addressing any gaps or concerns that arise.








# Our People


<details markdown="1">

<summary>Meet the Brand team</summary>

## Meet the Brand team

[**Luke Babb**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#luke)

* Title: Manager, Creative
* Email: luke@gitlab.com
* GitLab handle: @luke
* Slack handle: @luke

[**Matt Salik**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#msalik)

* Title: Senior Brand Designer
* Email: msalik@gitlab.com
* GitLab handle: @msalik
* Slack handle: @matt

[**Monica Galletto**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#monica_galletto)

* Title: Associate Production Designer
* Email: mgalletto@gitlab.com
* GitLab handle: @monica_galletto
* Slack handle: @monicagalletto

[**Vic Bell**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#vicbell)

* Title: Senior Illustrator
* Email: vbell@gitlab.com
* GitLab handle: @vicbell
* Slack handle: @vic

</details>


## Tools

<details markdown="1">

<summary>show/hide this section</summary>

- [Adobe Creative Cloud / Suite](https://www.adobe.com/): Adobe Creative Cloud is a set of applications and services from Adobe Inc. that gives subscribers access to a collection of software used for graphic design, video editing, web development, photography, along with a set of mobile applications and also some optional cloud services.
- [Sketch](https://www.sketch.com/): Create, prototype, collaborate and turn your ideas into incredible products with the definitive platform for digital design.
- [Mural](https://mural.co/): MURAL is an Online Virtual Collaboration Space, Easy to Use Specially Designed for Teams. You can Post Stickies, Share Ideas, Brainstorm and Run Product Sprints.
- [Sisense ( previously Periscope )](https://www.sisense.com/product/data-teams/): Sisense for Cloud Data Teams (previously Periscope Data) empowers data teams to quickly connect to cloud data sources, then explore and analyze data in a matter of minutes. Extend cloud investments with the Sisense analytics platform to build, embed, and deploy analytics at scale.
- [Launch Darkly](https://launchdarkly.com/): LaunchDarkly is a Feature Management Platform that serves over 100 billion feature flags daily to help software teams build better software, faster.
- [Swiftype](https://swiftype.com/): Swiftype is our search provider for the about site and handbook site. We are on a legacy "business" plan where we are allowed 100,000 documents to index, 3 engines, 24 hours for partial recrawls (edited documents), and 7 days for full recrawls (new & deleted documents).

</details>
