---
layout: handbook-page-toc
title: "GitLab's Customer Security Assessment Procedure"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose
It's no surprise that GitLab Customers and Prospects conduct Security and Risk Management due diligence activities prior to contracting with GitLab. We recognize the importance of these reviews and have designed this procedure to document the intake, tracking and responses to these assessments.

## Scope
The Customer Security Assessment Procedure is applicable to Security, Privacy and Legal due dilligence reviews conducted by GitLab Customers and Prospects. 

## Roles and Responsibilities

| Role | Responsibility |
| ------ | ------ |
| **Risk and Field Security team** | Maintain a mechanism to intake and respond to Customer Assessment requests |
| | Provide complete and accurate responses within documented SLA|
| | Document and report any risk or trends identified during Customer Assessment Activities |
| **Account Owner** | Recieve Customer or Prospect requests and submit to Risk and Field Security team |
| | Engage Solutions Architect or Technical Account Manager, as needed |
| | Provide information and documentation back to Customer or Prospect |
| **Solutions Architect** | Complete the First Pass of the questionnaire |
| **Current Customers** | Utilize Customer Self-Service tools noted below |
| |Contact your [Account Owner](/handbook/sales/#initial-account-owner---based-on-segment) at GitLab. If you don't know who that is, please reach out to [support@gitlab.com](mailto:support@gitlab.com) and ask to be connected to your Account Owner. |
| **Prospective Customers** | [Fill out a request](https://about.gitlab.com/sales/) and a representative will reach out to you. |


## Customer Self-Service Information Gathering

At GitLab, we are extremely [transparent](/handbook/values/#transparency) and the answers to many common questions may already be publicly available. However, we also [iterate](/handbook/values/#iteration) quickly, meaning the answers may change over time. We encourage our Customers and Prospects to utilize the below self-service resources as a first step in the process of assessing GitLab. 

* Search for [General Information about GitLab](https://about.gitlab.com) in our public handbook.
* Review [GitLab's Customer Assurance Package](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/customer-assurance-package.html)
* Review [GitLab's Product Security Documentation](https://docs.gitlab.com)

Should these resources not provide you with sufficient information, your Account Owner will follow the GitLab Assisted Information Gathering Process below. 

## GitLab Assisted Information Gathering
Despite GitLab's extereme transparency, there may be items our Customers and Prospects can't find using our Self Service resources above. Account Owners will facilitate these requests using the procedure below. 

### Step 1: Create a GitLab Issue
**Account Owner**
* Engage the appropriate Customer Success point of contact ([Solutions Architect](/handbook/customer-success/solutions-architects/#engaging-a-solutions-architect) or [Technical Account Manager](/handbook/customer-success/tam/#tam-responsibilities)) for the account
* Create, or request the Customer Success individual create, an [issue on the SA Triage Board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/) **using the Customer Security Assessment Template** 
* Ensure the Issue links to the **applicable opportunity, not the account** in Salesforce. 
   - **NOTE**: If opportunity is less than $5,000.00, the Risk and Field Security team will not provide input on the questionnaire unless otherwise prioritized through Sales and Security Leadership.
* Assign the issue to the Solutions Architect.

### Step 2: Complete the First Pass
**Solutions Architect**
* Begin by searching GitLab's **internal** database of commonly asked questions, [**GitLab AnswerBase**](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/common-security-questions.html). 
    - **NOTE**: Be sure to pay attention to the scope (SaaS or Self-Hosted) as well as answers that require an NDA prior to sharing. 
* If you can't find the answer in **GitLab AnswerBase**, ask your questions in Slack, [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70). You can also search for prior questions and answers in slack using the following format: `[keywords] in:[channel name]`
* Once you have answered as much as you can, ensure that the questionnaire is loaded into the [Security Questionnaire Folder ](https://drive.google.com/open?id=0B6GNv2pwhtCxWVJWdEZCTUEwbXc) and that the [Risk and Field Security team](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/#makeup-of-the-team) has edit access to the file.
* Ensure that a Due Date is set on the issue in line with the 10-day Service Level Agreement (SLA) for completion of Customer Security Assessments. If an expedited review is needed ensure you message the #sec-fieldsecurity slack channel
* Ensure the label `Security Ready for Review` is assigned

Once the label is assigned, the Risk and Field Security team will be notified. 

**GitLab Assisted Information Gathering High Level Diagram**

```mermaid
graph TD;
	A[Customer/Prospect:<br/> Review Self Service]-->
  B{Remaining Questions?};
  B--> C[No] & D[Yes]
  C --> Thanks!
  D --> E[Contact Account Owner]
  E--> F[Account Owner:<br/>Create GL Issue &<br/>Engage Solutions Architect]
  F--> G[Solutions Architect:<br/>Complete First Pass];
  G--> H{Remaining Questions?}
  H--> I[No] & J[Yes]
  I --> K[Thanks!]
  J--> L[Solutions Architect:<br/>Upload File];
	L--> M[Solutions Architect:<br/>Label Issue to notify Risk and Field Security];
```
### Step 3: Escalate to Risk and Field Security team
**Risk and Field Security (R&FS) Analyst**
* Review the issue and ensure all relevant information is provided. If it is not, remove the label `Security Ready for Review` and return it to the requester with feedback. Some common items that would lead to this are:
	* The requester didn't complete a First Pass using available resources
	* Required information such as Opportunity Tier, CRM link, or Link to Questionnaire are missing
	* The opportunity is below $5,000.00 ACV
	* The questions not appropriate for security (this often happens when customers ask financial and business operations information)
* Acknowledge receipt of the issue with a comment, apply the scoped label `Security Ready for Review::backlog` and assign it to the next R&FS team member in the rotation.
* Add the request to the [**Internal Only** metrics tracker](https://docs.google.com/spreadsheets/d/1nGlg_8PYRpEsr1bkrnTUijy6s3JDaA_Fls6YC9bssM4/edit?usp=sharing).
* Complete the questionnaire using the [**Internal Only** Customer Questions Standards](https://docs.google.com/document/d/1y5qP1X2ITUjWBJxoRpGwRPMZC-CHAODoTio15cC7DhY/edit?usp=sharing)
* If at any point in time, it becomes clear that desired completion date won't be met, inform relevant Parties.
* Once completed, remove the `Security Ready for Review` label and add the `SecurityReviewed` label. The R&FS team member should keep themselves assigned in case there are further questions. 

**Important NOTE**: Follow-on questions greater than 5 questions are considered a new questionnaire and the SLA will reset.

**Escalation Process High Level Diagram**
```mermaid
graph TD;
  st[Issue Labeled: 'Security Readyfor Review'];
  ac{Accept Issue?};
  ak[R&FS: Acknowledge Receipt and Assign];
  bl[Apply Label `Security Ready for Review::backlog`]
  cpy[R&FS: Complete and Return to requester];
  rt[R&FS: Return to requester with feedback];
  ul[R&FS: Remove 'Security Ready for Review'];
  uln[R&FS: Remove 'Security Ready for Review'];
  sl[R&FS: Label `Security Reviewed`];
  st -->|Issue Reviewed by R&FS|ac --> Yes & No;
  Yes -->ak --> bl -->|Within 10 cal days|cpy --> ul --> sl;
  No -->uln --> rt --> st;
  sl  --> Done!;
```

## Service Level Agreements
#### Account Owner
There is currently no published guidance on SLA from customer request to Account Owner response.

#### Solutions Architect
There is currently no published guidance on SLA from Account Owner assignment to response from Solutions Architect. 

#### Risk and Field Security
Once the Risk and Field Security team has accepted the issue, all efforts will be made to complete the questionnaire within **10 calendar days**. In some cases this may not be possible and these issues will be esclated to Security Assurance and Sales Leadership for prioritzation. 

In the event the Risk and Field Security team can not meet the requirements of the Sales Team it is important to update the Solutions Architect, Account Owner, and Security Leadership as soon as possible.

>>>
[Solutions Architect] and [Account Owner],

I'm sorry to inform you that due to [circumstances] we will be unable to meet your desired completion date of mm/dd/yyyy. We are truly sorry about this but we wanted to communicate and set expectations early.

Currently we estimate we can complete this request by [mm/dd/yyyy]. Please feel free to message in Slack [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) or you can DM me directly. I'm copying in my leadership to inform them of the issue.

In the interim, please have a look at our [Customer Assurance Package]()

We look forward to resolving this request.

/cc @mmaneval20, @julia.lake
>>>

## Exceptions
If the Account Owner or Customer Success point of contact feel they have sufficient knowledge and resoures to complete a Customer Assessment, this procedure does not have to used. These excpetions, will not be tracked. 

## References
Not applicable