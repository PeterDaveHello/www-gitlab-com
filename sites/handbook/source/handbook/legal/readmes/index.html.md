---
layout: handbook-page-toc
title: "GitLab Legal Team READMEs"
---

## Legal Team READMEs

- [Rob Nalen (Director of Legal Operations & Contracts)](https://about.gitlab.com/handbook/legal/readmes/robnalen.index.html)
